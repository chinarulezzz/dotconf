# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    BLACK="\[\033[0;30m\]"
    RED="\[\033[0;31m\]"
    GREEN="\[\033[0;32m\]"
    YELLOW="\[\033[0;33m\]"
    BLUE="\[\033[0;34m\]"
    MAGENTA="\[\033[0;35m\]"
    CYAN="\[\033[0;36m\]"
    WHITE="\[\033[0;37m\]"
    EMBLACK="\[\033[1;30m\]"
    EMRED="\[\033[1;31m\]"
    EMGREEN="\[\033[1;32m\]"
    EMYELLOW="\[\033[1;33m\]"
    EMBLUE="\[\033[1;34m\]"
    EMMAGENTA="\[\033[1;35m\]"
    EMCYAN="\[\033[1;36m\]"
    EMWHITE="\[\033[1;37m\]"
    BGBLACK="\[\033[40m\]"
    BGRED="\[\033[41m\]"
    BGGREEN="\[\033[42m\]"
    BGYELLOW="\[\033[43m\]"
    BGBLUE="\[\033[44m\]"
    BGMAGENTA="\[\033[45m\]"
    BGCYAN="\[\033[46m\]"
    BGWHITE="\[\033[47m\]"
    NORMAL='\[\033[00m\]'
  
    if [ ${EUID} == 0 ]; then
	PS1="\n${RED}┌─[ ${EMRED}\u${CYAN}@\h ${NORMAL}in ${GREEN}\w${NORMAL}${RED} ]\n└─# ${NORMAL}"
    else
	git_branch() {
	    _PS1=$(__git_ps1 2>/dev/null | sed 's/[()]//g')
	    [ ! -z "${_PS1}" ] && echo -e " ⎇\033[0;37m${_PS1}\033[00m"
        }
	PS1="\n${NORMAL}┌─[ ${EMBLUE}\u${CYAN}@\h ${NORMAL}in ${GREEN}\w${NORMAL}\$(git_branch) ]\n└─$ "
    fi
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ll='ls -lFh'
alias la='ls -lAhS'
alias l='ls -CF'
alias df='df -h'
alias du='du -h -c'
alias mkdir='mkdir -p -v'
alias ..='cd ..'
alias ...='cd ../..'
alias bc='bc -ql'
alias ls-sort-by-size='find . -maxdepth 1 -type f -exec du -h {} + | sort -r -h'
alias vim='vim -O'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# some functions
forget()	{ history -d $( history | awk 'END{print $1-1}' );	}
termtitle()	{ echo -ne "\033]0;${1}\007";				}
myip()		{ curl -s ipecho.net/plain;				}

hexdiff()	{ diff <(xxd $1) <(xxd $2);				}

bin2hex()	{ echo "obase=16;ibase=2; $1" | bc;			}
bin2dec()	{ echo "obase=10;ibase=2; $1" | bc;			}
dec2hex()	{ echo "obase=16;ibase=10;$1" | bc;			}
dec2bin()	{ echo "obase=2; ibase=10;$1" | bc;			}
hex2bin()	{ echo "obase=2; ibase=16;$(echo $1 | tr a-f A-F)" | bc;}
hex2dec()	{ echo "obase=10;ibase=16;$(echo $1 | tr a-f A-F)" | bc;}
str2hex()	{ echo $1 | xxd -p;					}
hex2str()	{ echo $1 | xxd -r -p;					}

chr()		{ printf \\$(printf '%03o' $1);				}
ord()		{ printf '%d' "'$1";					}

calc()		{ echo "$*" | bc -l;					}

psgrep()	{ ps aux | head -1 ; ps aux | grep $1 | grep -v grep;	}

tinyurl()	{ perl -MURI::Escape -MHTTP::Tiny -E \
    'say new HTTP::Tiny->get($ARGV[0].uri_escape($ARGV[1]))->{content}' \
    'http://tinyurl.com/api-create.php?url=' "$1"
}

transfer()	{ curl --upload-file "$1" "https://transfer.sh/$1";	}

email-extract() { perl -nle 'print for /(\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b)/g;' $1;    }
ip-extract()	{ perl -nle 'print $& if /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;' $1; }

show-suggests() { apt-cache depends show $1 | \
		  perl -F: -ne '/Suggests/ && do{print($F[1]) if $F[1] !~ /<.*>$/}';}

# End of file
